(async  function () {
  // render prompt if not using Beaker
  if (!navigator.userAgent.includes('BeakerBrowser')) {
    renderUAPrompt()
    return
  }

  // setup
  let archive, archiveInfo, folders
  let selectedSnippets = []

  try {
    archive = new DatArchive(window.location)
    archiveInfo = await archive.getInfo()
  } catch (err) {
    updatePrompt('<a onclick="window.location.reload()">Something went wrong.</a>')
  }

  const foldersData = window.localStorage.getItem('folders')
  if (foldersData) {
    folders = JSON.parse(foldersData)
  } else {
    folders = []
    window.localStorage.setItem('folders', '[]')
  }

  renderApp()

  // events

  async function onCreateFolder (e) {
    // create a new Dat archive
    const folder = await DatArchive.create()

    // create the /snippets directory
    await folder.mkdir('/snippets')

    // write the folder's URL to localStorage
    folders.push(folder.url)
    window.localStorage.setItem('folders', JSON.stringify(folders))

    // write the folder's assets
    const html = await archive.readFile('folder.html')
    await folder.writeFile('index.html', html)
    await folder.writeFile('readme.md', `#${folder.name}`)
    await folder.commit()

    // go to the new archive
    window.location = folder.url
  }

  async function onDeleteFolder (e) {
    e.preventDefault()
    e.stopPropagation()

    const url = e.target.dataset.folder

    // remove the folder element from DOM
    document.querySelector('.folder-container').removeChild(document.querySelector(`a.folder[href="${url}"]`))

    // remove folder URL from storage
    folders.splice(folders.indexOf(url), 1)
    window.localStorage.setItem('folders', JSON.stringify(folders))
  }

  // renderers

  function renderApp () {
    // clear the prompt
    updatePrompt('')

    document.querySelectorAll('.create-folder').forEach(el => el.addEventListener('click', onCreateFolder))

    renderFolders()
  }

  function renderFolders () {
    for (let i = 0; i < folders.length; i++) {
      appendFolder(new DatArchive(folders[i]))
    }
  }

  async function appendFolder (folder) {
    const info = await folder.getInfo()
    let folderHTML = ''

    // get all of the snippets in the folder
    const snippets = await folder.readdir('/snippets')

    // create the folder element
    const el = document.createElement('a')
    el.classList.add('folder')
    el.href = folder.url

    folderHTML += `
      <div class="dropdown" data-folder="${folder.url}">
        <div class="delete-folder-btn" data-folder="${folder.url}">Delete folder</div>
      </div>
    `
    
    folderHTML += `<div class="placeholder">${snippets.length}</div>`

    // add the title
    folderHTML += `<div class="title">${info.title || '<em>Untitled</em>'}</div>`

    // add the snippet count to the HTML
    let pluralize = ''
    if (snippets.length !== 1) pluralize = 's'
    folderHTML += `<div class="snippet-count">${snippets.length} snippet${pluralize}</div>`

    el.innerHTML += folderHTML

    // create dropdown button
    const dropdownBtn = document.createElement('span')
    dropdownBtn.classList.add('dropdown-btn')
    dropdownBtn.title = 'Show folder menu'
    dropdownBtn.dataset.folder = folder.url
    dropdownBtn.innerText = '▾'
    dropdownBtn.addEventListener('click', toggleFolderDropdown)
    el.appendChild(dropdownBtn)

    document.querySelector('.folder-container').appendChild(el)

    document.querySelectorAll('.delete-folder-btn').forEach(function (el) {
      el.addEventListener('click', onDeleteFolder)
    })
  }

  function renderUAPrompt () {
    updatePrompt('<p>Sorry >.< This app only works in the Beaker Browser. </p><a class="btn primary" href="https://beakerbrowser.com/docs/install/">Install Beaker</a>')
  }

  // helpers

  function toggleFolderDropdown (e) {
    e.preventDefault()
    e.stopPropagation()
    console.log(e.target.dataset.folder)
    document.querySelector(`.dropdown[data-folder="${e.target.dataset.folder}"]`).classList.toggle('visible')
  }

  function updatePrompt (html) {
    if (typeof html !== 'string') return
    if (html.length) {
      document.querySelector('#prompt').innerHTML = `<div class="content">${html}</div>`
    } else {
      document.querySelector('#prompt').innerHTML = html
    }
  }
})()
