(async  function () {
  // render prompt if not using Beaker
  if (!navigator.userAgent.includes('BeakerBrowser')) {
    renderUAPrompt()
    return
  }

  // setup
  let archive, archiveInfo, folders
  let selectedSnippets = []

  const urlEl = document.getElementById('url')
  const shareBtn = document.getElementById('share-btn')
  shareBtn.addEventListener('click', onShare)

  try {
    archive = new DatArchive(window.location)
    archiveInfo = await archive.getInfo()

    // remove header if not archive owner
    if (!archiveInfo.isOwner) {
      document.body.removeChild(document.querySelector('header'))
    }

    // Write folder title and description if set
    // TODO allow user to edit this after creating the folder
    document.title = archiveInfo.title || 'Untitled'
    if (archiveInfo.description) document.title += `- ${archiveInfo.description}`

    document.querySelector('h1').innerHTML = archiveInfo.title || '<em>Untitled</em>'
    document.querySelector('.desc').innerText = archiveInfo.description || ''

    // set value of hidden textarea to album's URL
    urlEl.innerHTML = archive.url
  } catch (err) {
    updatePrompt('<a onclick="window.location.reload()">Something went wrong.</a>')
  }

  renderApp()

  // events

  function onShare () {
    urlEl.select()
    document.execCommand('copy')
    updatePrompt(`<div id="close-btn" class="btn">Close</div><h3 style=flex:1;>Share your folder's URL</h2><p><code>${archive.url}</code></p><p><em>URL copied to clipboard</em></p>`)

    const closeBtn = document.getElementById('close-btn')
    closeBtn.addEventListener('click', function () {
      updatePrompt('')
    })
  }

  function onToggleSelected (e) {
    e.target.classList.toggle('selected')

    const path = e.target.getAttribute('data-path')
    const idx = selectedSnippets.indexOf(path)

    // either add or remove the path to selectedSnippets
    if (idx === -1) selectedSnippets.push(path)
    else selectedSnippets.splice(idx, 1)
  }

  async function onDeleteSelected () {
    for (let i = 0; i < selectedSnippets.length; i++) {
      const path = selectedSnippets[i]

      // remove from DOM
      document.querySelector(`[data-path='${path}']`).remove()

      // remove from archive
      await archive.unlink(selectedSnippets[i])
    }
    await archive.commit()
  }

  function onEditInfo () {
    // TODO
    // replace the h1 and description with inputs

    // add a save button

    // add an event listener to the save button
  }

  // renderers

  function renderApp () {
    // clear the prompt
    updatePrompt('')
    renderFolder()

    document.getElementById('more-btn').addEventListener('click', function (e) {
      document.querySelector('.more-dropdown').classList.toggle('visible')
    })

    // TODO
    // document.getElementById('edit-info').addEventListener('click', onEditInfo)
    document.getElementById('delete-selected').addEventListener('click', onDeleteSelected)

    document.getElementById('add-btn').addEventListener('click', function () {
      updatePrompt(`<div id="close-btn" class="btn">Close</div><h3 style=flex:1;>Create a new snippet</h2><form id=snippet-form><div class=header><input name=path placeholder="Filename including extension"></div><textarea name=content></textarea></form><div id=create-btn class=btn style=width:80px>Create</div>`)

      const closeBtn = document.getElementById('close-btn')
      closeBtn.addEventListener('click', function () {
        updatePrompt('')
        return
      })  

      const createBtn = document.getElementById('create-btn')
      createBtn.addEventListener('click', async function () {
        var form = document.getElementById("snippet-form")
        updatePrompt('')
        var path = form.path.value
        var content = form.content.value

        if (path && content) {
          await archive.writeFile("/snippets/"+path, content)
          appendSnippet(path)
        }
        await archive.commit()
      })
    })
  }

  async function renderFolder () {
    try {
      const paths = await archive.readdir('snippets')

      // TODO sort by ctime or mtime
      for (let i = 0; i < paths.length; i++) {
        appendSnippet(paths[i])
      }
    } catch (err) {
      updatePrompt('<p>Something went wrong</p>')
      console.error(err)
    }
  }

  function renderUAPrompt () {
    updatePrompt('<p>Sorry >.< This app only works in the Beaker Browser. </p><a class="btn primary" href="https://beakerbrowser.com/docs/install/">Install Beaker</a>')
  }

  function appendSnippet(path) {
    if (typeof path !== 'string') return

    const el = document.createElement('a')
    el.classList.add('snippet-container')
    el.setAttribute("data-path", path)
    el.href = `/snippets/${path}`
    //el.addEventListener('click', onToggleSelected)
    el.innerHTML = `<p style=word-wrap:break-word;word-break:break-word;>${path}</p>`;
    document.querySelector('.folder-snippets').appendChild(el)
  }

  // helpers

  function updatePrompt (html) {
    if (typeof html !== 'string') return
    if (html.length) {
      document.querySelector('#prompt').innerHTML = `<div class="content">${html}</div>`

    } else {
      document.querySelector('#prompt').innerHTML = html
    }
  }
})()
