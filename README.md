# Nugget is a decentralized, peer-to-peer paste tool

*Save and share snippets completely independently. No third party required.*

## Instructions

With [Beaker](https://beakerbrowser.com), open the app's URL:

On Hashbase: `dat://nugget.hashbase.io`

Raw URL: `dat://5eee3ef544d54182e36e1d789d2d16508ec0c69e5f596978818cba85330b2896`

All snippets are stored locally on your device, and are only ever visible to 
people with whom you share the folders's secret URL.
